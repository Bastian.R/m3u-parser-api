
# m3u-parser-api

Api javascript pour parser un fichier m3u selon son url. 




## Tech Stack

Node.js, express, body parser, nodemon, javascript.


## Prerequisites

Appel vers un url de fichier M3U étendu uniquement.

Le fichier M3U étendu doit contenir des lignes #EXTINF ( Extended information field ) avec les attributs : tvg-id, tvg-name, tvg-logo.

En cas de manquement des attributs sur une ligne #EXTINF le process n'est pas stoppé.
## Routes

POST : /getMediaItems

## Deployment

Pour démarrer le projet : 

```bash
 cd desktop
 git clone https://gitlab.com/Bastian.R/m3u-parser-api.git
 cd desktop/m3u-parser-api
 npm i
 npm run 
 npm run dev
```


## Usage/Examples

```javascript
const fetchChannels = async (m3u8Url) => {
     const response = await fetch("http://localhost:5000/getMediaItems", {
        method: "POST",
        mode: "cors",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ link: m3u8Url }),
      });
      const data = await response.json();
}
```


## Data/Examples

```javascript
[
    {tvgId: "", tvgName: "", tvgLogo: ""},
    {tvgId: "", tvgName: "", tvgLogo: ""},
    {tvgId: "", tvgName: "", tvgLogo: ""}
]
```

## Licence CC BY-NC-ND

This license enables reusers to copy and distribute the material in any medium or format in unadapted form only, for noncommercial purposes only, and only so long as attribution is given to the creator. 

CC BY-NC-ND includes the following elements:

 BY: credit must be given to the creator.

 NC: Only noncommercial uses of the work are permitted.

 ND: No derivatives or adaptations of the work are permitted.
## Authors

- [@Bastian.R](https://gitlab.com/Bastian.R/)

