async function getMediaItems(link) {
  const response = await fetch(link);
  let data = await response.text();

  data = data
    .split("#EXTINF:-1")
    .filter(function (line) {
      if (
        line.includes(".m3u8") ||
        line.includes(".mp4") ||
        line.includes(".avi")
      ) {
        return line;
      }
    })
    .join("#EXTINF:-1");

  data = extractValuesFromM3uText(data.split("\n"));
  return data;
}

function extractValuesFromM3uText(lines) {
  let result = [];
  const regexTvgId = /tvg-id="([^"]*)"/;
  const regexTvgName = /tvg-name="([^"]*)"/;
  const regexTvgLogo = /tvg-logo="([^"]*)"/;
  const regexGroupTitle = /group-title="([^"]*)"/;
  lines.forEach((element) => {
    element = element.split("\n");
    if (element[0].startsWith("#EXTINF:-1")) {
      result.push({
        tvgId: element[0].match(regexTvgId)?.[1]
          ? element[0].match(regexTvgId)[1]
          : "No Id",
        tvgName: element[0].match(regexTvgName)?.[1]
          ? element[0].match(regexTvgName)[1]
          : "No Name",
        tvgLogo: element[0].match(regexTvgLogo)?.[1]
          ? element[0].match(regexTvgLogo)[1]
          : "No Logo",
        groupTitle: element[0].match(regexGroupTitle)?.[1]
          ? element[0].match(regexGroupTitle)[1]
          : "No Group",
      });
    }
    if (element[0].startsWith("http") && result.length > 0) {
      result[result.length - 1].url = element[0];
    }
  });
  return result;
}

module.exports = { getMediaItems };
