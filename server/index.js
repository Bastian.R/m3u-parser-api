const express = require("express");
const PORT = 5000;
const app = express();
const bodyParser = require("body-parser");
const fetch = require("node-fetch");
const cors = require("cors");
const { getMediaItems } = require("./services/M3uParserService");

//Middleware
app.use(bodyParser.json());
app.use(cors());

//Routes
app.post("/getMediaItems", async function (req, res) {
  const body = req.body;
  res.set("Content-Type", "application/json");
  const data = await getMediaItems(body.link);
  res.send(JSON.stringify(data));
});

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
